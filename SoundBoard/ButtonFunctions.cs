﻿using System;
using System.Windows.Forms;
using System.Media;

namespace SoundBoard {
    class ButtonFunctions {

        private static SoundPlayer[] audioFiles = { null, null, null, null, null, null, null, null, null, null };
        private static XMLReader xmlReader = new XMLReader();
        private string OpenFileExplorer(OpenFileDialog openFileDialog)
        {
            openFileDialog.ShowDialog();
            return openFileDialog.FileName;
        }

        public void UpdateButton(Button b, int buttonID)
        {
            UpdateButton(b, buttonID, null, false);
        }

        public void UpdateButton(Button b, int buttonID, OpenFileDialog openFileDialog, bool edit)
        {
            if (edit)
            {
                string file = OpenFileExplorer(openFileDialog);
                audioFiles[buttonID - 1] = new SoundPlayer(file);
                string[] a = file.Split('\\');
                b.Text = a[a.Length - 1];
                xmlReader.SaveToFile(buttonID, file);
            }
            else if (!edit)
            {
                if (audioFiles[buttonID - 1] != null)
                {
                    audioFiles[buttonID - 1].Play();
                }
            }
        }

        public void OnLoad(Button b, int buttonID)
        {
            string file = xmlReader.ReadFromFile(buttonID);
            try
            {
                if (file != "Empty")
                {
                    audioFiles[buttonID - 1] = new SoundPlayer(file);
                    string[] a = file.Split('\\');
                    b.Text = a[a.Length - 1];
                }
                else
                {
                    audioFiles[buttonID - 1] = null;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
