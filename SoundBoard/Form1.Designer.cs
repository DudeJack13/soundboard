﻿namespace SoundBoard
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.backgroundTimer = new System.Windows.Forms.Timer(this.components);
            this.tableButtonLayout = new System.Windows.Forms.TableLayoutPanel();
            this.soundButton1 = new System.Windows.Forms.Button();
            this.soundButton2 = new System.Windows.Forms.Button();
            this.soundButton3 = new System.Windows.Forms.Button();
            this.soundButton4 = new System.Windows.Forms.Button();
            this.soundButton5 = new System.Windows.Forms.Button();
            this.soundButton6 = new System.Windows.Forms.Button();
            this.soundButton7 = new System.Windows.Forms.Button();
            this.soundButton8 = new System.Windows.Forms.Button();
            this.soundButton9 = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.editButton = new System.Windows.Forms.Button();
            this.tableButtonLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "notifyIcon1";
            this.notifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseDoubleClick);
            // 
            // backgroundTimer
            // 
            this.backgroundTimer.Enabled = true;
            this.backgroundTimer.Tick += new System.EventHandler(this.backgroundTimer_Tick);
            // 
            // tableButtonLayout
            // 
            this.tableButtonLayout.AutoSize = true;
            this.tableButtonLayout.ColumnCount = 3;
            this.tableButtonLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableButtonLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableButtonLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableButtonLayout.Controls.Add(this.soundButton1, 0, 0);
            this.tableButtonLayout.Controls.Add(this.soundButton2, 1, 0);
            this.tableButtonLayout.Controls.Add(this.soundButton3, 2, 0);
            this.tableButtonLayout.Controls.Add(this.soundButton4, 0, 1);
            this.tableButtonLayout.Controls.Add(this.soundButton5, 1, 1);
            this.tableButtonLayout.Controls.Add(this.soundButton6, 2, 1);
            this.tableButtonLayout.Controls.Add(this.soundButton7, 0, 2);
            this.tableButtonLayout.Controls.Add(this.soundButton8, 1, 2);
            this.tableButtonLayout.Controls.Add(this.soundButton9, 2, 2);
            this.tableButtonLayout.Location = new System.Drawing.Point(12, 12);
            this.tableButtonLayout.Name = "tableButtonLayout";
            this.tableButtonLayout.RowCount = 3;
            this.tableButtonLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableButtonLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableButtonLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableButtonLayout.Size = new System.Drawing.Size(318, 318);
            this.tableButtonLayout.TabIndex = 0;
            // 
            // soundButton1
            // 
            this.soundButton1.Location = new System.Drawing.Point(3, 3);
            this.soundButton1.Name = "soundButton1";
            this.soundButton1.Size = new System.Drawing.Size(99, 99);
            this.soundButton1.TabIndex = 0;
            this.soundButton1.Text = "button1";
            this.soundButton1.UseVisualStyleBackColor = true;
            this.soundButton1.Click += new System.EventHandler(this.soundButton1_Click);
            // 
            // soundButton2
            // 
            this.soundButton2.Location = new System.Drawing.Point(108, 3);
            this.soundButton2.Name = "soundButton2";
            this.soundButton2.Size = new System.Drawing.Size(100, 99);
            this.soundButton2.TabIndex = 1;
            this.soundButton2.Text = "button2";
            this.soundButton2.UseVisualStyleBackColor = true;
            this.soundButton2.Click += new System.EventHandler(this.soundButton2_Click);
            // 
            // soundButton3
            // 
            this.soundButton3.Location = new System.Drawing.Point(214, 3);
            this.soundButton3.Name = "soundButton3";
            this.soundButton3.Size = new System.Drawing.Size(100, 99);
            this.soundButton3.TabIndex = 2;
            this.soundButton3.Text = "button3";
            this.soundButton3.UseVisualStyleBackColor = true;
            this.soundButton3.Click += new System.EventHandler(this.soundButton3_Click);
            // 
            // soundButton4
            // 
            this.soundButton4.Location = new System.Drawing.Point(3, 109);
            this.soundButton4.Name = "soundButton4";
            this.soundButton4.Size = new System.Drawing.Size(99, 99);
            this.soundButton4.TabIndex = 3;
            this.soundButton4.Text = "button4";
            this.soundButton4.UseVisualStyleBackColor = true;
            this.soundButton4.Click += new System.EventHandler(this.soundButton4_Click);
            // 
            // soundButton5
            // 
            this.soundButton5.Location = new System.Drawing.Point(108, 109);
            this.soundButton5.Name = "soundButton5";
            this.soundButton5.Size = new System.Drawing.Size(100, 99);
            this.soundButton5.TabIndex = 4;
            this.soundButton5.Text = "button5";
            this.soundButton5.UseVisualStyleBackColor = true;
            this.soundButton5.Click += new System.EventHandler(this.soundButton5_Click);
            // 
            // soundButton6
            // 
            this.soundButton6.Location = new System.Drawing.Point(214, 109);
            this.soundButton6.Name = "soundButton6";
            this.soundButton6.Size = new System.Drawing.Size(100, 99);
            this.soundButton6.TabIndex = 5;
            this.soundButton6.Text = "button6";
            this.soundButton6.UseVisualStyleBackColor = true;
            this.soundButton6.Click += new System.EventHandler(this.soundButton6_Click);
            // 
            // soundButton7
            // 
            this.soundButton7.Location = new System.Drawing.Point(3, 215);
            this.soundButton7.Name = "soundButton7";
            this.soundButton7.Size = new System.Drawing.Size(99, 100);
            this.soundButton7.TabIndex = 6;
            this.soundButton7.Text = "button7";
            this.soundButton7.UseVisualStyleBackColor = true;
            this.soundButton7.Click += new System.EventHandler(this.soundButton7_Click);
            // 
            // soundButton8
            // 
            this.soundButton8.Location = new System.Drawing.Point(108, 215);
            this.soundButton8.Name = "soundButton8";
            this.soundButton8.Size = new System.Drawing.Size(100, 100);
            this.soundButton8.TabIndex = 7;
            this.soundButton8.Text = "button8";
            this.soundButton8.UseVisualStyleBackColor = true;
            this.soundButton8.Click += new System.EventHandler(this.soundButton8_Click);
            // 
            // soundButton9
            // 
            this.soundButton9.Location = new System.Drawing.Point(214, 215);
            this.soundButton9.Name = "soundButton9";
            this.soundButton9.Size = new System.Drawing.Size(100, 100);
            this.soundButton9.TabIndex = 8;
            this.soundButton9.Text = "button9";
            this.soundButton9.UseVisualStyleBackColor = true;
            this.soundButton9.Click += new System.EventHandler(this.soundButton9_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // editButton
            // 
            this.editButton.Location = new System.Drawing.Point(251, 336);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(75, 23);
            this.editButton.TabIndex = 1;
            this.editButton.Text = "Edit";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(345, 488);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.tableButtonLayout);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.tableButtonLayout.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.Timer backgroundTimer;
        private System.Windows.Forms.TableLayoutPanel tableButtonLayout;
        private System.Windows.Forms.Button soundButton1;
        private System.Windows.Forms.Button soundButton2;
        private System.Windows.Forms.Button soundButton3;
        private System.Windows.Forms.Button soundButton4;
        private System.Windows.Forms.Button soundButton5;
        private System.Windows.Forms.Button soundButton6;
        private System.Windows.Forms.Button soundButton7;
        private System.Windows.Forms.Button soundButton8;
        private System.Windows.Forms.Button soundButton9;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button editButton;
    }
}

