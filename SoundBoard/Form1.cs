﻿using System;
using System.Windows.Forms;

namespace SoundBoard
{
    public partial class Form1 : Form
    {
        private InterceptKeys interceptKeys;
        private ButtonFunctions buttonFunctions;
        private bool editMode = false;
        private bool controlHold = false;

        public Form1()
        {
            InitializeComponent();
            interceptKeys = new InterceptKeys();
            buttonFunctions = new ButtonFunctions();

            buttonFunctions.OnLoad(soundButton1, 1);
            buttonFunctions.OnLoad(soundButton2, 2);
            buttonFunctions.OnLoad(soundButton3, 3);
            buttonFunctions.OnLoad(soundButton4, 4);
            buttonFunctions.OnLoad(soundButton5, 5);
            buttonFunctions.OnLoad(soundButton6, 6);
            buttonFunctions.OnLoad(soundButton7, 7);
            buttonFunctions.OnLoad(soundButton8, 8);
            buttonFunctions.OnLoad(soundButton9, 9);
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (WindowState != FormWindowState.Normal) {
                Hide();
                notifyIcon.Visible = true;
            }
        }

        private void notifyIcon_MouseDoubleClick(object sender, MouseEventArgs e) 
        {
            Show();
            WindowState = FormWindowState.Normal;
            notifyIcon.Visible = false;
        }

        private void backgroundTimer_Tick(object sender, EventArgs e)
        {
            //162 - Left Control
            //103 l 104 l 105
            //100 l 101 l 102
            //97 l 98 l 99
            int input = interceptKeys.GetInputs();
            Console.WriteLine(input);
            if (input == 162) //Control
            {
                controlHold = true;
            }
            else if (input == interceptKeys.GetNoInputValue())
            {
                controlHold = false;
            }

            if (controlHold)
            {
                if (input == 103) { buttonFunctions.UpdateButton(soundButton1, 1); }
                else if (input == 104) { buttonFunctions.UpdateButton(soundButton1, 2); }
                else if (input == 105) { buttonFunctions.UpdateButton(soundButton1, 3); }
                else if (input == 100) { buttonFunctions.UpdateButton(soundButton1, 4); }
                else if (input == 101) { buttonFunctions.UpdateButton(soundButton1, 5); }
                else if (input == 102) { buttonFunctions.UpdateButton(soundButton1, 6); }
                else if (input == 97) { buttonFunctions.UpdateButton(soundButton1, 7); }
                else if (input == 98) { buttonFunctions.UpdateButton(soundButton1, 8); }
                else if (input == 99) { buttonFunctions.UpdateButton(soundButton1, 9); }
            }
        }
        private void editButton_Click(object sender, EventArgs e)
        {
            if (!editMode)
            {
                editMode = true;
                editButton.Text = "Exit Edit";
            }
            else if (editMode)
            {
                editMode = false;
                editButton.Text = "Edit";
            }
        }

        private void soundButton1_Click(object sender, EventArgs e)
        {
            buttonFunctions.UpdateButton(soundButton1, 1, openFileDialog, editMode);
        }

        private void soundButton2_Click(object sender, EventArgs e)
        {
            buttonFunctions.UpdateButton(soundButton2, 2, openFileDialog, editMode);
        }

        private void soundButton3_Click(object sender, EventArgs e)
        {
            buttonFunctions.UpdateButton(soundButton3, 3, openFileDialog, editMode);
        }

        private void soundButton4_Click(object sender, EventArgs e)
        {
            buttonFunctions.UpdateButton(soundButton4, 4, openFileDialog, editMode);
        }

        private void soundButton5_Click(object sender, EventArgs e)
        {
            buttonFunctions.UpdateButton(soundButton5, 5, openFileDialog, editMode);
        }

        private void soundButton6_Click(object sender, EventArgs e)
        {
            buttonFunctions.UpdateButton(soundButton6, 6, openFileDialog, editMode);
        }

        private void soundButton7_Click(object sender, EventArgs e)
        {
            buttonFunctions.UpdateButton(soundButton7, 7, openFileDialog, editMode);
        }

        private void soundButton8_Click(object sender, EventArgs e)
        {
            buttonFunctions.UpdateButton(soundButton8, 8, openFileDialog, editMode);
        }

        private void soundButton9_Click(object sender, EventArgs e)
        {
            buttonFunctions.UpdateButton(soundButton9, 9, openFileDialog, editMode);
        }
    }
}