﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;

namespace SoundBoard {
    class XMLReader {
        public void SaveToFile(int id, string fileLocation)
        {
            XmlDocument root = new XmlDocument();
            root.Load("audioMapping.xml");

            foreach (XmlNode e in root.GetElementsByTagName("button"))
            {
                if (e.Attributes["id"].Value.Equals(id.ToString()))
                {
                    e.FirstChild.Value = fileLocation; 
                    break;
                }
            }
            root.Save("audioMapping.xml");
        }
        
        public string ReadFromFile(int id)
        {
            XmlDocument root = new XmlDocument();
            root.Load("audioMapping.xml");

            foreach (XmlNode e in root.GetElementsByTagName("button"))
            {
                if (e.Attributes["id"].Value.Equals(id.ToString()))
                {
                    return e.FirstChild.Value;
                }
            }
            return "error";
        }
    }
}
