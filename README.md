# SoundBoard

Creating a sound board that will allow audio files to be played over a microphone using a global hotkey.

## Progress: ##
**Todo:**<br>
- Create way to adjust audio levels<br>
- UI design<br>

**Finished:** <br>
- Get key in input while running in background<br>
- Close to system tray<br>
- Set up buttons to set and play custom audio<br>
- Create hotkeys inputs to trigger audio<br>
